<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('welcome', function () {
    return view('welcome');
});

route::get('master', function(){
    return view('adminlte.master');
});

route::get('items', function(){
    return view('items.index');
});

route::get('items/create', function(){
    return view('items.create');
});

route::get('/', function(){
    return view('items.route1');
});

route::get('/data-tables', function(){
    return view('items.route2');
});

?>